package ninja.sedzik.server;


import com.google.gson.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;



public class Server {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;


    private long startTime = System.nanoTime();
    private long endTime;
    private long totalTime;


    public void init() throws IOException {
        serverSocket = new ServerSocket(6666);
        System.out.println("Server is running in port 6666");
        clientSocket = serverSocket.accept();


        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        while (true) {
            switch (in.readLine()) {
                case "uptime":
                    getUptime();
                    break;
                case "info":
                    showInfo();
                    break;
                case "help":
                    showHelp();
                    break;
                case "stop":
                    stop();
                    return;
                default:
                    out.println("Unknown command");
            }
        }

    }

    private void stop() throws IOException {
        in.close();
        out.close();
        System.out.println("client is just stopping");
        clientSocket.close();
        serverSocket.close();
        System.out.println("Server is stopped");
    }


        void getUptime() {
            endTime = System.nanoTime();
            totalTime = (endTime - startTime) / 1000000000;
            String uptime = String.valueOf(totalTime);
            JsonObject response = new JsonObject();
            response.addProperty("uptime: ",  uptime + " seconds");
            out.println(response);
        }


    private void showInfo() {
        JsonObject response = new JsonObject();
        response.addProperty("info", "server version 1.0 created in 2021");
        out.println(response);
    }

    private void showHelp() {
        JsonObject response = new JsonObject();
        response.addProperty("uptime", "returns how long server is started.");
        response.addProperty("info", "returns version and creation date of the server.");
        response.addProperty("help", "returns all available comments and description");
        response.addProperty("stop", "stops server and client");
        out.println(response);
    }


    public static void main(String[] args) throws IOException {
        Server server = new Server();
        server.init();
    }


}
